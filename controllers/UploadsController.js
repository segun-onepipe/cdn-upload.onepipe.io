const express = require('express');
const path = require('path');

const router = express.Router();

router.get('/', (req, res) => {

  return res.json({ status: true, data: {}, message: 'Files retrieved successfully' });

});

router.post('/', (req, res) => {

  try {

    if(!req.files || Object.keys(req.files).length === 0)
      return res.status(400).json({status: false, message: 'Please select a file to upload '});
    
    if(!req.files.file)
      return res.status(400).json({status: false, message: 'file key is required'});

    let file = req.files.file;
    let destination = path.join(__dirname, '../', `/assets/${file.name}`);

    file.mv(destination, function(err){

      if(err)
        return res.status(500).json({status: false, message: err});
      
      return res.json({
        status: true, 
        message: 'File successfully uploaded', 
        data: {

          file_url: destination
        }
      });
   
    });
  
  } catch (ex) {

    res .status(500).json({ status: false, data: {}, message: ex.message });
  }
});

module.exports = router;
