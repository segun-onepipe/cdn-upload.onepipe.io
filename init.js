const express = require('express');
const bodyparser = require('body-parser');
const fileUpload = require('express-fileupload');

const upload = require('./controllers/UploadsController');

module.exports = function (app) {
  app.use(express.json());
  app.use(bodyparser.urlencoded({ extended: true }));
  app.use(fileUpload());
  app.use(function (req, res, next) {

    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With', 'Content-Type, Accept');

    next();
  });

  app.use('/', upload);

  app.use(function (err, req, res) {

    return res.status(500).json({ status: false, error: err });
  });
};
